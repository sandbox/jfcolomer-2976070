<?php

module_load_include('inc', 'context', 'plugins/context_condition');

class context_nodequeue_context_condition_nodequeue extends context_condition {

  function condition_values() {
    $values = array();
    foreach (context_nodequeue_load_queues() as $key=> $queue) {
      $values[$queue->name] = $queue->title;
    }
    return $values;
  }

  function execute($node) {
    foreach ($this->get_contexts() as $context) {
      $queues = $this->fetch_from_context($context);
      if (!empty($queues['values'])) {
        if (context_nodequeue_nid_in_queue($node, $queues['values'])) {
          $this->condition_met($context, TRUE);
        }
      }
    }
  }
}
